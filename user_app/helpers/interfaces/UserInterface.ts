export interface UserInterface {
  id: string;
  name: string;
  full_name: string;
  age: number;
  city: string;
  tag: string;
  url: string;
  knowledge: {
    language: string;
    frameworks: string[];
  }[];
}
