import React, { Component } from 'react'
import Head from 'next/head'
import { getDataFromTree } from 'react-apollo'
import { ApolloClient, InMemoryCache, HttpLink, NormalizedCacheObject } from 'apollo-boost';
import fetch from 'isomorphic-unfetch';
import { API_URL } from '_env';

let apolloClient = null;

if (typeof window !== 'undefined') {
  global.fetch = fetch
}

const createClient = (initialState: NormalizedCacheObject) => {
  return new ApolloClient({
    connectToDevTools: typeof window === 'undefined',
    ssrMode: typeof window !== 'undefined',
    link: new HttpLink({
      uri: `http://${API_URL}/graphql`,
      credentials: 'same-origin'
    }),
    cache: new InMemoryCache().restore(initialState || {})
  })
};

const getApolloClient = (initialState: NormalizedCacheObject) => {
  if (typeof window !== 'undefined') {
    return createClient(initialState);
  }
  if (!apolloClient) {
    apolloClient = createClient(initialState);
  }
  return apolloClient;
};


export default (App) => {
  return class ApolloConnect extends Component {
    static displayName = 'ApolloConnect(App)';
    static async getInitialProps (ctx) {
      const { Component, router } = ctx;
      const isServer = typeof window === 'undefined';
      let appProps = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }
      const apollo = getApolloClient({});
      if (isServer) {
        try {
          await getDataFromTree(
            <App
              {...appProps}
              Component={Component}
              router={router}
              apolloClient={apollo}
            />
          )
        } catch (error) {
          console.error('Error while running `getDataFromTree`', error)
        }
        try {
          Head.rewind();
        } catch(e) {}
      }
      const apolloState = apollo.cache.extract();
      return {
        ...appProps,
        apolloState
      }
    }
    apolloClient = null;
    constructor (props) {
      super(props);
      this.apolloClient = getApolloClient(props.apolloState);
      this.apolloClient.initQueryManager();
      if (typeof window !== 'undefined') {
        // @ts-ignore
        window.__APOLLO_CLIENT__ = this.apolloClient;
      }
    }
    render () {
      return <App {...this.props} apolloClient={this.apolloClient} />
    }
  }
}
