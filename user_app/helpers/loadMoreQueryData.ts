export const loadMoreQueryData = (propertyName: string, variables: any, fetchMore: (...args) => void) => {
  fetchMore({
    variables,
    updateQuery: (previousResult, { fetchMoreResult }) => {
      if (!fetchMoreResult) { return previousResult; }
      return Object.assign({}, previousResult, {
        [propertyName]: [...previousResult[propertyName], ...fetchMoreResult[propertyName]]
      });
    }
  })
};
