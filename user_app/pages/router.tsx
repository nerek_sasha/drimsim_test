import React, { Component } from 'react';
import { BrowserRouter, StaticRouter, Route } from 'react-router-dom';

import Index from './index';
import UserPage from './userPage';

export interface RouterPropsInterface {
  location: string;
  context: any;
}

export interface RouterStateInterface {
  isServer: boolean;
}

class Router extends Component<RouterPropsInterface, RouterStateInterface> {
  static async getInitialProps ({ req }) {
    if (!req) {
      return {};
    }
    const { url } = req;
    return {
      context: {},
      location: url
    };
  }
  constructor(props){
    super(props);
    this.state.isServer = typeof window === 'undefined';
  }
  state = { isServer: null };
  renderRoutes() {
    return (
      <div>
        <Route exact path="/" component={Index} />
        <Route path="/user/:id" component={UserPage} />
      </div>
    );
  }
  render () {
    const Router = this.state.isServer ? StaticRouter : BrowserRouter;
    const routerProps = this.state.isServer ? this.props : {};
    return (
      <Router {...routerProps} >
        {this.renderRoutes()}
      </Router>
    );
  }
}

export default Router;
