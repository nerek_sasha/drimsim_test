import Head from 'next/head';
import React from 'react';
import App, { AppProps, Container, DefaultAppIProps } from 'next/app';
import { ApolloClient } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { ApolloConnect } from 'helpers/index';

import 'normalize.css/normalize.css';
import 'bootstrap/dist/css/bootstrap-grid.css';

export interface ApplicationContainerInterface extends DefaultAppIProps, AppProps {
  apolloClient: ApolloClient<{}>;
}

class ApplicationContainer extends App<ApplicationContainerInterface> {
  render () {
    const { Component, pageProps, apolloClient } = this.props;
    return (
      <Container>
        <Head>
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
        </Head>
        <ApolloProvider client={apolloClient}>
          <Component {...pageProps} />
        </ApolloProvider>
      </Container>
    );
  }
}

export default ApolloConnect(
  ApplicationContainer
);
