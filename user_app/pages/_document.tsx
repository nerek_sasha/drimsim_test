import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';

class MyDocument extends Document {
  getChildContext() {
    delete this.props.__NEXT_DATA__.props.pageProps.apollo_state;
    return {
      _documentProps: this.props
    };
  }

  render() {
    return (
      <html>
      <Head />
      <body>
      <Main />
      <NextScript />
      <script
        type="text/javascript"
        dangerouslySetInnerHTML={{
          __html: `window.__APOLLO_STATE__ = ${
            JSON.stringify(this.props.__NEXT_DATA__.props.pageProps.apollo_state || {})
            };`
        }}
      />
      </body>
      </html>
    );
  }
}

export default MyDocument;
