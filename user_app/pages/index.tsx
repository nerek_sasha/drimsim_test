import Head from 'next/head';
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo'
import React, { Component } from 'react';

import { loadMoreQueryData } from 'helpers';
import { UserInterface } from 'helpers/interfaces';
import { allUsers } from '_graphql/queries';

import 'styles/styles.scss';

export interface IndexPropsInterface {}

export interface IndexStateInterface {
  variables: any;
}

class Index extends Component<IndexPropsInterface, IndexStateInterface> {
  state = {
    variables: {}
  };
  renderLoadMoreButton(isMore: boolean, isLoading: boolean, fetchMore: (...args) => void) {
    if (isMore) {
      return (
        <button
          onClick={() => loadMoreQueryData('allUsers', {}, fetchMore)}
        >
          {!isLoading ? 'Show More' : 'Loading...'}
        </button>
      );
    }
    return null;
  }
  renderItem(user: UserInterface) {
    const { id, name, full_name, age, knowledge } = user;
    const knowledgeString = knowledge.map(({ language }) => language).join(', ');
    return (
      <Link
        key={id}
        to={`/user/${id}`}
      >
        <div className="card">
          <div className="card__avatar">
            <div
              className="card__avatar-image"
              style={{
                backgroundImage: `url(/static/avatar.jpg)`
              }}
            />
            <span className="card__text-main">
                {name || full_name}
              </span>
            <span className="card__text-secondary">
                {age} years
              </span>
            <span className="card__text-secondary card__text-other-info">
                {knowledgeString}
              </span>
          </div>
        </div>
      </Link>
    );
  }
  render () {
    const { variables } = this.state;
    return (
      <div className="index">
        <Head>
          <title>Index Page</title>
          <meta charSet='utf-8' />
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        </Head>
        <main>
          <Query query={allUsers} variables={variables || {}} fetchPolicy="cache-and-network">
            {({ loading, error, data: { allUsers, totalUsers }, fetchMore }) => {
              if (error) return error.message;
              if (loading) return (<div>Loading</div>);
              const isMore = +allUsers.length < +totalUsers;
              return (
                <section className="container">
                  <div
                    className="cards"
                  >
                    {allUsers.map((user: UserInterface) => this.renderItem(user))}
                    {this.renderLoadMoreButton(isMore, loading, fetchMore)}
                  </div>
                </section>
              )
            }}
          </Query>
        </main>
      </div>
    );
  }
}

export default Index;
