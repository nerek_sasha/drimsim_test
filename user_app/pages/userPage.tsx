import Head from 'next/head';
import {
Redirect
} from "react-router-dom";
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Query, Mutation } from 'react-apollo';

import { user as userQuery } from '_graphql/queries';
import { updateUserCity } from '_graphql/mutation';
import { UserInterface } from 'helpers/interfaces';

import 'styles/styles.scss';

export interface UserPagePropsInterface {
  match: {
    params: {
      id: number;
    };
  };
}

export interface UserPageStateInterface {
  isEdit: boolean;
  variables: {
    userID: string;
    city: string;
  };
}

class UserPage extends Component<UserPagePropsInterface, UserPageStateInterface> {
  state = {
    isEdit: false,
    variables: {
      userID: null,
      city: null,
    }
  };
  renderForm() {
    return (
      <div className="row align-content-center">
        <div className="col justify-content-center d-flex">
          <Mutation
            mutation={updateUserCity}
            update={(cache, { data: { updateUserCity: { city } } }) => {
              const data = cache.readQuery({
                query: userQuery,
                variables: {
                  id: this.props.match.params.id
                }
              });
              cache.writeQuery({
                query: userQuery,
                data: {
                  ...data,
                  user: {
                    // @ts-ignore
                    ...data.user,
                    city,
                  }
                }
              });
            }}
          >
            {(updateUserCity) => (
              <form
                className="form"
                onSubmit={(e) => {
                  e.preventDefault();
                  this.setState({ isEdit: false });
                  if (this.state.variables.userID) {
                    updateUserCity({variables: this.state.variables});
                  }
                }}
              >
                <label>
                  Country:
                  <input
                    type="text"
                    name="city"
                    defaultValue={this.state.variables.city}
                    onChange={({ target: { value, name }}) => {
                      this.setState({
                        variables: {
                          ...this.state.variables,
                          [name]: value
                        }
                      });
                    }}
                  />
                </label>
                <input
                  type="submit"
                  className="action"
                  value="Submit"
                />
              </form>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
  renderProfileInfo(user: UserInterface) {
    const { id, name, full_name, age, knowledge, city } = user;
    return (
      <div
        key={id}
        className="col align-self-center align-items-start"
      >
        <div className="card">
          <div className="card__avatar">
            <div
              className="card__avatar-image"
              style={{
                backgroundImage: `url(/static/avatar.jpg)`
              }}
            />
            <span className="card__text-main">
              {name || full_name}
            </span>
            <span className="card__text-secondary">
              {age} years
            </span>
            {(() => {
              if (this.state.isEdit) {
                return this.renderForm();
              }
              return (
                <span className="card__text-secondary card__text-other-info action">
                    <span className="tooltip"
                          onClick={() => this.setState({
                            isEdit: true,
                            variables: {
                              userID: id,
                              city: city || '',
                            }
                          })}
                    >
                      {city || (<span className="card__text-secondary action">Enter City</span>)}
                      <span className="tooltip-text">Click to edit</span>
                    </span>
                  </span>
              );
            })()}
            <hr />
            {
              knowledge.map(({ language, frameworks }, index) => {
                const frameworksString = frameworks.join(' - ');
                const keyString = language.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
                return [
                  <span
                    key={`lang_${keyString}_${index}`}
                    className="card__text-main card__text-main-secondary"
                  >
                    {language}
                  </span>,
                  <span
                    key={`frameworks_${keyString}_${index}`}
                    className="card__text-secondary card__text-other-info"
                  >
                    {frameworksString}
                  </span>
                ];
              })
            }
          </div>
        </div>
      </div>
    );
  }
  render () {
    if (
      !this.props.match
      || !this.props.match.params
      || !this.props.match.params.id
      || !Number.isInteger(+this.props.match.params.id)
    ) {
      // @ts-ignore
      return (
        <Redirect to="/" />
      );
    }
    return (
      <div>
        <Head>
          <title>User Page</title>
          <meta charSet='utf-8' />
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        </Head>
        <main className="profile">
          <Query query={userQuery} variables={{ id: this.props.match.params.id }}>
            {({ loading, error, data: { user }}) => {
              if (error) return error.message;
              if (loading) return (<div>Loading</div>);
              return (
                <section className="container">
                  <div className="row">
                    <div className="col">
                      <Link
                        className="arrow"
                        to="/"
                      >
                        <span className="arrow__head" />
                        <span className="arrow__body" />
                        <span className="arrow__text action">
                          Back
                        </span>
                      </Link>
                    </div>
                  </div>
                  <div className="row justify-content-center">
                    {this.renderProfileInfo(user)}
                  </div>
                </section>
              )
            }}
          </Query>
        </main>
      </div>
    );
  }
}

export default UserPage;
