import gql from 'graphql-tag';

export const User = gql`
  fragment User on User {
    id,
    name,
    full_name,
    age,
    city,
    tag,
    url,
    knowledge{
      language,
      frameworks
    }
  }
`;
