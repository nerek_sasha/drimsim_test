import gql from 'graphql-tag';
// import { User } from '_graphql/fragments';

export const updateUserCity = gql`
  mutation updateUserCity($userID: ID!, $city: String!){
    updateUserCity(userID: $userID,  city: $city) {
      city
    }
  }
`;
