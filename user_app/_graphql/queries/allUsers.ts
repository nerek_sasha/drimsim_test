import gql from 'graphql-tag';
import { User } from '_graphql/fragments';

export const allUsers = gql`
  query {
    allUsers {
      ...User
    },
    totalUsers
  }
  ${User}
`;
