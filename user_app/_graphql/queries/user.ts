import gql from 'graphql-tag'
import { User } from '_graphql/fragments';

export const user = gql`
  query user($id: Int!) {
    user(id: $id) {
      ...User
    }
  }
  ${User}
`;
