const UrlPrettifier = require('next-url-prettifier').default;

const routes = [
  { page: 'router', prettyUrl: '/' },
  { page: 'router', prettyUrl: ({ id }) => `/user/${id}`, prettyUrlPatterns: [{ pattern: '/user/:id' }] }
];

const urlPrettifier = new UrlPrettifier(routes);
exports.default = routes;
exports.Router = urlPrettifier;
