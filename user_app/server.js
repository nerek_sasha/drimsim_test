const express = require('express');
const next = require('next');
const Router = require('./routes').Router;
require('dotenv').config();
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
app.prepare()
  .then(() => {
    const server = express();

    Router.forEachPrettyPattern((page, pattern, defaultParams) => server.get(pattern, (req, res) => {
        return app.render(req, res, `/${page}`, Object.assign({}, defaultParams, req.query, req.params))
    }));

    server.get('*', (req, res) => handle(req, res));
    server.listen(port)
  });
