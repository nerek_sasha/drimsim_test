declare module '_env' {
  export const API_URL: string;
  export const LOCAL_API_URL: string;
  export const PORT: number;
  export const GA_KEY: string;
  export const GA_TAG_KEY: string;
  export const IS_SECURE_CONNECTION: number;
}
