declare module '*.gql' {
  import { DocumentNode } from '_graphql';
  const value: DocumentNode;
  export = value;
}
