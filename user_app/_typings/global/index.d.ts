declare namespace NodeJS {
  interface Global {
    fetch: Function,
    XMLHttpRequest: XMLHttpRequest;
  }
}
